<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FuncionarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('funcionarios')) {

            Schema::create('funcionarios', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('id_usuario');
                $table->integer('codigo')->unique();
                $table->timestamps();

                $table->foreign('id_usuario')->references('id')->on('usuarios');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionarios');
    }
}
