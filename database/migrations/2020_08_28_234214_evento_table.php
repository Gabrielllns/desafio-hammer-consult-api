<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EventoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('eventos')) {

            Schema::create('eventos', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('id_usuario_responsavel');
                $table->string('titulo', 150);
                $table->text('descricao');
                $table->dateTime('data_realizacao');
                $table->boolean('st_ativo')->default(true);
                $table->timestamps();

                $table->foreign('id_usuario_responsavel')->references('id')->on('usuarios');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
