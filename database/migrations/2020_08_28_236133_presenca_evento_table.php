<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PresencaEventoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('presenca_eventos')) {

            Schema::create('presenca_eventos', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('id_evento');
                $table->unsignedBigInteger('id_usuario');
                $table->boolean('st_bebe')->default(false);
                $table->timestamps();

                $table->foreign('id_evento')->references('id')->on('eventos');
                $table->foreign('id_usuario')->references('id')->on('usuarios');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presenca_eventos');
    }
}
