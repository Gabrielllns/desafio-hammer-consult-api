<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PerfilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('perfis')) {

            Schema::create('perfis', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('descricao', 50);
                $table->boolean('st_ativo')->default(true);
                $table->boolean('st_empresa');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfis');
    }
}
