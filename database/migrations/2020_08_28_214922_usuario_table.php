<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('usuarios')) {

            Schema::create('usuarios', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('id_perfil');
                $table->string('nome', 100);
                $table->boolean('st_ativo')->default(true);
                $table->timestamps();

                $table->foreign('id_perfil')->references('id')->on('perfis');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
