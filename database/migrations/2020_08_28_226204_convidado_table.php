<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConvidadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('convidados')) {

            Schema::create('convidados', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('id_usuario');
                $table->unsignedBigInteger('id_funcionario_responsavel');
                $table->timestamps();

                $table->foreign('id_usuario')->references('id')->on('usuarios');
                $table->foreign('id_funcionario_responsavel')->references('id')->on('funcionarios');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convidados');
    }
}
