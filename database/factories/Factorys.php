<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Perfil::class, function (Faker $faker) {
    return [];
});

$factory->define(App\Models\Usuario::class, function (Faker $faker) {
    return [
        'nome' => $faker->name
    ];
});

$factory->define(App\Models\Funcionario::class, function (Faker $faker) {
    return [];
});

$factory->define(App\Models\Convidado::class, function (Faker $faker) {
    return [];
});

$factory->define(App\Models\Evento::class, function (Faker $faker) {
    return [
        'titulo' => $faker->text(100),
        'descricao' => $faker->text(),
        'data_realizacao' => $faker->dateTime
    ];
});

$factory->define(App\Models\PresencaEvento::class, function (Faker $faker) {
    return [];
});
