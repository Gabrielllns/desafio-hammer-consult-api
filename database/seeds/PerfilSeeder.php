<?php

use \App\Models\Perfil;
use Illuminate\Database\Seeder;

class PerfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Perfil::class)->create([
            'id' => 1,
            'descricao' => 'Gerente',
            'st_empresa' => true
        ]);

        factory(Perfil::class)->create([
            'id' => 2,
            'descricao' => 'Funcionário',
            'st_empresa' => true
        ]);

        factory(Perfil::class)->create([
            'id' => 3,
            'descricao' => 'Convidado',
            'st_empresa' => false
        ]);
    }
}
