<?php

use \App\Models\PresencaEvento;
use Illuminate\Database\Seeder;

class PresencaEventoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PresencaEvento::class)->create([
            'id' => 1,
            'id_evento' => 1,
            'id_usuario' => 2,
        ]);

        factory(PresencaEvento::class)->create([
            'id' => 2,
            'id_evento' => 1,
            'id_usuario' => 3,
            'st_bebe' => true
        ]);

        factory(PresencaEvento::class)->create([
            'id' => 3,
            'id_evento' => 1,
            'id_usuario' => 4,
        ]);
    }
}
