<?php

use App\Models\Funcionario;
use Illuminate\Database\Seeder;

class FuncionarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Funcionario::class)->create([
            'id' => 1,
            'id_usuario' => 1,
            'codigo' => 2020001
        ]);

        factory(Funcionario::class)->create([
            'id' => 2,
            'id_usuario' => 2,
            'codigo' => 2020002
        ]);

        factory(Funcionario::class)->create([
            'id' => 3,
            'id_usuario' => 3,
            'codigo' => 2020003
        ]);
    }
}
