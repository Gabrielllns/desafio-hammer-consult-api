<?php

use \App\Models\Evento;
use Illuminate\Database\Seeder;

class EventoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Evento::class)->create([
            'id' => 1,
            'id_usuario_responsavel' => 1
        ]);

        factory(Evento::class)->create([
            'id' => 2,
            'id_usuario_responsavel' => 1
        ]);
    }
}
