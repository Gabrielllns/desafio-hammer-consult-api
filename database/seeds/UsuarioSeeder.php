<?php

use App\Models\Usuario;
use Illuminate\Database\Seeder;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Usuario::class)->create([
            'id' => 1,
            'id_perfil' => 1
        ]);

        factory(Usuario::class)->create([
            'id' => 2,
            'id_perfil' => 2
        ]);

        factory(Usuario::class)->create([
            'id' => 3,
            'id_perfil' => 2
        ]);

        factory(Usuario::class)->create([
            'id' => 4,
            'id_perfil' => 3
        ]);

        factory(Usuario::class)->create([
            'id' => 5,
            'id_perfil' => 3
        ]);
    }
}
