<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Tabelas de Domínio
        $this->call(PerfilSeeder::class);

        // Demais Tabelas
        $this->call(UsuarioSeeder::class);
        $this->call(FuncionarioSeeder::class);
        $this->call(ConvidadoSeeder::class);
        $this->call(EventoSeeder::class);
        $this->call(PresencaEventoSeeder::class);
    }
}
