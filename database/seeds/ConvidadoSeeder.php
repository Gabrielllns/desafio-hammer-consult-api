<?php

use App\Models\Convidado;
use Illuminate\Database\Seeder;

class ConvidadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Convidado::class)->create([
            'id_usuario' => 4,
            'id_funcionario_responsavel' => 2
        ]);

        factory(Convidado::class)->create([
            'id_usuario' => 5,
            'id_funcionario_responsavel' => 3
        ]);
    }
}
