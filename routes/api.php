<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/api/documentation');
});

Route::prefix('perfis')->group(function () {
    Route::get('/', 'PerfilController@getPerfis');
    Route::get('/empresa', 'PerfilController@getPerfisEmpresa');
    Route::get('/{id}', 'PerfilController@getPerfilPorId');
});

Route::prefix('usuarios')->group(function () {
    Route::get('/', 'UsuarioController@getUsuarios');
    Route::get('/{id}', 'UsuarioController@getUsuarioPorId');
});

Route::prefix('funcionarios')->group(function () {
    Route::get('/', 'FuncionarioController@getFuncionarios');
    Route::get('/{id}', 'FuncionarioController@getFuncionarioPorId');
    Route::get('/pendentes/evento/{id}', 'FuncionarioController@getFuncionarioPendentesPorEvento');
    Route::get('/confirmados/evento/{id}', 'FuncionarioController@getFuncionariosConfirmadosPorEvento');
    Route::get('/confirmados/evento/{id}/total', 'FuncionarioController@getTotalFuncionariosConfirmadosPorEvento');
    Route::post('/', 'FuncionarioController@salvar');
});

Route::prefix('convidados')->group(function () {
    Route::get('/', 'ConvidadoController@getConvidados');
    Route::get('/{id}', 'ConvidadoController@getConvidadoPorId');
    Route::get('/confirmados/evento/{id}', 'ConvidadoController@getConvidadosConfirmadosPorEvento');
    Route::get('/confirmados/evento/{id}/total', 'ConvidadoController@getTotalConvidadosConfirmadosPorEvento');
    Route::post('/', 'ConvidadoController@salvar');
});

Route::prefix('eventos')->group(function () {
    Route::get('/', 'EventoController@getEventos');
    Route::get('/{id}', 'EventoController@getEventoPorId');
    Route::post('/', 'EventoController@salvar');
});

Route::prefix('presencas-eventos')->group(function () {
    Route::post('/', 'PresencaEventoController@salvar');
    Route::get('/evento/{id}/estatistica', 'PresencaEventoController@getEstatisticasEvento');
    Route::get('/evento/{id}/total-gasto', 'PresencaEventoController@getTotalGastoEvento');
    Route::get('/evento/{id}/total-gasto/comida', 'PresencaEventoController@getTotalGastoComidaEvento');
    Route::get('/evento/{id}/total-gasto/bebida', 'PresencaEventoController@getTotalGastoBebidaEvento');
    Route::get('/evento/{id}/total-participantes', 'PresencaEventoController@getTotalConfirmadosEvento');
    Route::get('/evento/{id}/total-participantes/convidado', 'PresencaEventoController@getTotalConvidadosConfirmadosEvento');
    Route::get('/evento/{id}/total-participantes/funcionario', 'PresencaEventoController@getTotalFuncionariosConfirmadosEvento');
    Route::put('/{id}/cancelar', 'PresencaEventoController@cancelar');
});
