<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PresencaEvento
 *
 * @package App\Models
 * @author Gabrielllns
 *
 * @OA\Schema(title="PresencaEventoModel", description="Responsável pelo modelo de PresencaEvento.")
 */
class PresencaEvento extends Model
{
    /**
     * Define o nome da tabela.
     *
     * @var string
     */
    protected $table = 'presenca_eventos';

    /**
     * Define a utilização dos timestemps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Define os atributos que podem ser manipulados.
     *
     * @var array
     */
    protected $fillable = [
        'id_evento',
        'id_usuario',
        'st_bebe'
    ];

    /**
     * Define o 'cast' dos atributos.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_evento' => 'integer',
        'id_usuario' => 'integer',
        'st_bebe' => 'boolean'
    ];

    /**
     * Define os atributos que devem ser ocultados.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Define a relação de 'presença-evento' e 'evento'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function evento()
    {
        return $this->belongsTo(Evento::class, 'id_evento', 'id');
    }

    /**
     * Define a relação de 'presença-evento' e 'usuário'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function usuario()
    {
        return $this->hasOne(Usuario::class, 'id', 'id_usuario');
    }
}
