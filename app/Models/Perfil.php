<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Perfil
 *
 * @package App\Models
 * @author Gabrielllns
 *
 * @OA\Schema(title="PerfilModel", description="Responsável pelo modelo de Perfil.")
 */
class Perfil extends Model
{
    /**
     * Define o nome da tabela.
     *
     * @var string
     */
    protected $table = 'perfis';

    /**
     * Define a utilização dos timestemps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Define os atributos que podem ser manipulados.
     *
     * @var array
     */
    protected $fillable = [
        'descricao',
        'st_ativo',
        'st_empresa'
    ];

    /**
     * Define o 'cast' dos atributos.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'descricao' => 'string',
        'st_empresa' => 'boolean',
        'st_ativo' => 'boolean'
    ];

    /**
     * Define os atributos que devem ser ocultados.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Define a relação de 'perfis' e 'usuário'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'id');
    }
}
