<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Funcionario
 *
 * @package App\Models
 * @author Gabrielllns
 *
 * @OA\Schema(title="FuncionarioModel", description="Responsável pelo modelo de Funcionário.")
 */
class Funcionario extends Model
{
    /**
     * Define o nome da tabela.
     *
     * @var string
     */
    protected $table = 'funcionarios';

    /**
     * Define a utilização dos timestemps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Define os atributos que podem ser manipulados.
     *
     * @var array
     */
    protected $fillable = [
        'id_usuario',
        'codigo'
    ];

    /**
     * Define o 'cast' dos atributos.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_usuario' => 'integer',
        'codigo' => 'integer'
    ];

    /**
     * Define os atributos que devem ser ocultados.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Define a relação de 'funcionário' e 'usuário'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function usuario()
    {
        return $this->hasOne(Usuario::class, 'id', 'id_usuario');
    }

    /**
     * Define a relação de 'funcionário' e 'convidado'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function convidado()
    {
        return $this->hasOne(Convidado::class, 'id_funcionario_responsavel', 'id');
    }
}
