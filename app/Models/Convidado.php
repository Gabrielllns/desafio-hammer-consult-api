<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Convidado
 *
 * @package App\Models
 * @author Gabrielllns
 *
 * @OA\Schema(title="ConvidadoModel", description="Responsável pelo modelo de Convidado.")
 */
class Convidado extends Model
{
    /**
     * Define o nome da tabela.
     *
     * @var string
     */
    protected $table = 'convidados';

    /**
     * Define a utilização dos timestemps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Define os atributos que podem ser manipulados.
     *
     * @var array
     */
    protected $fillable = [
        'id_usuario',
        'id_funcionario_responsavel'
    ];

    /**
     * Define o 'cast' dos atributos.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_usuario' => 'integer',
        'id_funcionario_responsavel' => 'integer'
    ];

    /**
     * Define os atributos que devem ser ocultados.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Define a relação de 'convidado' e 'usuário'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function usuario()
    {
        return $this->hasOne(Usuario::class, 'id', 'id_usuario');
    }

    /**
     * Define a relação de 'convidado' e 'funcionário'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function funcionarioResponsavel()
    {
        return $this->hasOne(Funcionario::class, 'id', 'id_funcionario_responsavel');
    }
}
