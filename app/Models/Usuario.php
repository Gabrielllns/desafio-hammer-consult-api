<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Usuario
 *
 * @package App\Models
 * @author Gabrielllns
 *
 * @OA\Schema(title="UsuarioModel", description="Responsável pelo modelo de Usuário.")
 */
class Usuario extends Model
{
    /**
     * Define o nome da tabela.
     *
     * @var string
     */
    protected $table = 'usuarios';

    /**
     * Define a utilização dos timestemps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Define os atributos que podem ser manipulados.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'id_perfil',
        'st_ativo'
    ];

    /**
     * Define o 'cast' dos atributos.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'id_perfil' => 'integer',
        'st_ativo' => 'boolean',
    ];

    /**
     * Define os atributos que devem ser ocultados.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Define a relação de 'usuário' e 'perfil'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function perfil()
    {
        return $this->hasOne(Perfil::class, 'id', 'id_perfil');
    }

    /**
     * Define a relação de 'usuário' e 'funcionário'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function funcionario()
    {
        return $this->belongsTo(Funcionario::class, 'id');
    }

    /**
     * Define a relação de 'usuário' e 'convidado'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function convidado()
    {
        return $this->belongsTo(Convidado::class, 'id');
    }

    /**
     * Define a relação de 'usuário' e 'presença-evento'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function presencaEvento()
    {
        return $this->belongsTo(PresencaEvento::class, 'id', 'id_usuario');
    }

    /**
     * Define a relação de 'usuário' e 'eventos'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventos()
    {
        return $this->hasMany(Evento::class, 'id', 'id_usuario_responsavel');
    }
}
