<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Evento
 *
 * @package App\Models
 * @author Gabrielllns
 *
 * @OA\Schema(title="EventoModel", description="Responsável pelo modelo de Evento.")
 */
class Evento extends Model
{
    /**
     * Define o nome da tabela.
     *
     * @var string
     */
    protected $table = 'eventos';

    /**
     * Define a utilização dos timestemps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Define os atributos que podem ser manipulados.
     *
     * @var array
     */
    protected $fillable = [
        'id_usuario_responsavel',
        'titulo',
        'descricao',
        'data_realizacao',
        'st_ativo'
    ];

    /**
     * Define o 'cast' dos atributos.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_usuario_responsavel' => 'integer',
        'titulo' => 'string',
        'descricao' => 'text',
        'data_realizacao' => 'datetime',
        'st_ativo' => 'boolean'
    ];

    /**
     * Define os atributos que devem ser ocultados.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Define a relação de 'evento' e 'usuário'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function usuario()
    {
        return $this->hasOne(Usuario::class, 'id', 'id_usuario_responsavel');
    }

    /**
     * Define a relação de 'evento' e 'presença-evento'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function presencaEvento()
    {
        return $this->hasMany(PresencaEvento::class, 'id_evento', 'id');
    }
}
