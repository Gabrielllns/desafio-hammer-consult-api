<?php

namespace App\Util;

class Message
{
    const EMAIL_SENHA_INVALIDOS = "E-mail e/ou senha incorretos.";
    const REGISTROS_NAO_ENCONTRADOS = "Registros não encontrados.";
    const DADOS_INFORMADOS_INCORRETOS = "Os dados informados estão incorretos.";
}
