<?php

namespace App\Util;

class Utils
{
    /**
     * Retorna a request formatada.
     *
     * @param array $dados
     * @return array
     */
    public static function getRequestData($dados)
    {
        return $dados['data'];
    }
}
