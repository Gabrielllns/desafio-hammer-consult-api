<?php

namespace App\Util;

/**
 * Class Constants.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class Constants
{
    /**
     * Constante de Tipos de Perfis
     */
    const ID_PERFIL_GERENTE = 1;
    const ID_PERFIL_FUNCIONARIO = 2;
    const ID_PERFIL_CONVIDADO = 3;

    /**
     * Constante de um usuário responsável
     */
    const ID_USUARIO_RESPONSAVEL = 1;
}
