<?php

namespace App\To;

/**
 * Classe TO responsável pelo mapeamento das estatísticas dos eventos.
 *
 * @author Gabrielllns
 * @package App\To
 */
class EstatisticaEventoTO
{

    /**
     * @var integer
     */
    public $totalFuncionariosConfirmados = 0;

    /**
     * @var integer
     */
    public $totalConvidadosConfirmados = 0;

    /**
     * @var integer
     */
    public $totalArrecadado = 0;

    /**
     * @var integer
     */
    public $totalGasto = 0;

    /**
     * @var integer
     */
    public $totalGastoComida = 0;

    /**
     * @var integer
     */
    public $totalGastoBebida = 0;

    /**
     * @return integer
     */
    public function getTotalFuncionariosConfirmados()
    {
        return $this->totalFuncionariosConfirmados;
    }

    /**
     * @param integer $totalFuncionariosConfirmados
     */
    public function setTotalFuncionariosConfirmados($totalFuncionariosConfirmados)
    {
        $this->totalFuncionariosConfirmados = $totalFuncionariosConfirmados;
    }

    /**
     * @return integer
     */
    public function getTotalConvidadosConfirmados()
    {
        return $this->totalConvidadosConfirmados;
    }

    /**
     * @param integer $totalConvidadosConfirmados
     */
    public function setTotalConvidadosConfirmados($totalConvidadosConfirmados)
    {
        $this->totalConvidadosConfirmados = $totalConvidadosConfirmados;
    }

    /**
     * @return integer
     */
    public function getTotalArrecadado()
    {
        return $this->totalArrecadado;
    }

    /**
     * @param integer $totalArrecadado
     */
    public function setTotalArrecadado($totalArrecadado)
    {
        $this->totalArrecadado = $totalArrecadado;
    }

    /**
     * @return integer
     */
    public function getTotalGasto()
    {
        return $this->totalGasto;
    }

    /**
     * @param integer $totalGasto
     */
    public function setTotalGasto($totalGasto)
    {
        $this->totalGasto = $totalGasto;
    }

    /**
     * @return integer
     */
    public function getTotalGastoComida()
    {
        return $this->totalGastoComida;
    }

    /**
     * @param integer $totalGastoComida
     */
    public function setTotalGastoComida($totalGastoComida)
    {
        $this->totalGastoComida = $totalGastoComida;
    }

    /**
     * @return integer
     */
    public function getTotalGastoBebida()
    {
        return $this->totalGastoBebida;
    }

    /**
     * @param integer $totalGastoBebida
     */
    public function setTotalGastoBebida($totalGastoBebida)
    {
        $this->totalGastoBebida = $totalGastoBebida;
    }

    /**
     * @return false|string
     */
    public function compact()
    {
        return json_encode($this);
    }
}