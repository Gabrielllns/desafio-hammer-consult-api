<?php

namespace App\Http\Controllers;

use App\Business\PerfilBO;

/**
 * Class PerfilController
 *
 * @package App\Http\Controllers
 * @author Gabrielllns
 *
 * @OA\Tag(name="Perfis", description="Grupo de rotas referentes aos perfis.")
 */
class PerfilController extends Controller
{
    /**
     * @var PerfilBO
     */
    private $perfilBO;

    /**
     * PerfilController constructor.
     *
     * @param PerfilBO $perfilBO
     */
    public function __construct(PerfilBO $perfilBO)
    {
        $this->perfilBO = $perfilBO;
    }

    /**
     * @OA\Get(
     *     path="/perfis",
     *     operationId="getPerfis",
     *     tags={"Perfis"},
     *     description="Retorna todos os perfis cadastrados.",
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Perfil")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     */
    public function getPerfis()
    {
        return $this->perfilBO->getPerfis();
    }

    /**
     * @OA\Get(
     *     path="/perfis/empresa",
     *     operationId="getPerfisEmpresa",
     *     tags={"Perfis"},
     *     description="Retorna todos os perfis vinculados a empresa cadastrados.",
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Perfil")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     */
    public function getPerfisEmpresa()
    {
        return $this->perfilBO->getPerfisEmpresa();
    }

    /**
     * @OA\Get(
     *     path="/perfis/{id}",
     *     operationId="getPerfilPorId",
     *     tags={"Perfis"},
     *     description="Retorna o perfil cadastrado por meio do 'id' informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Perfil")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     */
    public function getPerfilPorId($id)
    {
        return $this->perfilBO->getPerfilPorId($id);
    }
}
