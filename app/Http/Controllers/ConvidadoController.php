<?php

namespace App\Http\Controllers;

use App\Business\ConvidadoBO;
use App\Models\Convidado;
use Illuminate\Http\Request;

/**
 * Class ConvidadoController
 *
 * @package App\Http\Controllers
 * @author Gabrielllns
 *
 * @OA\Tag(name="Convidados", description="Grupo de rotas referentes aos convidados.")
 */
class ConvidadoController extends Controller
{
    /**
     * @var ConvidadoBO
     */
    private $convidadoBO;

    /**
     * ConvidadoController constructor.
     *
     * @param ConvidadoBO $convidadoBO
     */
    public function __construct(ConvidadoBO $convidadoBO)
    {
        $this->convidadoBO = $convidadoBO;
    }

    /**
     * @OA\Post(
     *     path="/convidados",
     *     operationId="salvar",
     *     tags={"Convidados"},
     *     description="Salva os dados de convidado.",
     *     @OA\Parameter(name="funcionario", in="path", required=true, @OA\Schema(type="Perfil")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Convidado")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param Request $request
     * @return Convidado
     */
    public function salvar(Request $request)
    {
        $funcionario = $request->only(['data']);

        return $this->convidadoBO->salvar($funcionario);
    }

    /**
     * @OA\Get(
     *     path="/convidados",
     *     operationId="getConvidados",
     *     tags={"Convidados"},
     *     description="Retorna todos os convidados cadastrados.",
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Convidado")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @return Convidado[]
     */
    public function getConvidados()
    {
        return $this->convidadoBO->getConvidados();
    }

    /**
     * @OA\Get(
     *     path="/convidados/{id}",
     *     operationId="getConvidadoPorId",
     *     tags={"Convidados"},
     *     description="Retorna o convidado cadastrado por meio do 'id' informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Convidado")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return Convidado
     */
    public function getConvidadoPorId($id)
    {
        return $this->convidadoBO->getConvidadoPorId($id);
    }

    /**
     * @OA\Get(
     *     path="/convidados/confirmados/evento/{id}",
     *     operationId="getConvidadosConfirmadosPorEvento",
     *     tags={"Convidados"},
     *     description="Retorna a lista de convidados confirmados conforme o 'id' do evento informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object|array", ref="#/components/schemas/Convidado")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return Convidado[]
     */
    public function getConvidadosConfirmadosPorEvento($id)
    {
        return $this->convidadoBO->getConvidadosConfirmadosPorEvento($id);
    }

    /**
     * @OA\Get(
     *     path="/convidados/confirmados/evento/{id}/total",
     *     operationId="getTotalConvidadosConfirmadosPorEvento",
     *     tags={"Convidados"},
     *     description="Retorna o total de convidados confirmados conforme o 'id' do evento informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="integer")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return integer
     */
    public function getTotalConvidadosConfirmadosPorEvento($id)
    {
        return $this->convidadoBO->getTotalConvidadosConfirmadosPorEvento($id);
    }
}
