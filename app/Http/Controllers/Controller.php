<?php

namespace App\Http\Controllers;

use App\Util\Utils;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class Controller
 *
 * @package App\Http\Controllers
 * @author Gabrielllns
 *
 * @OA\Info(
 *     version="1.0.0",
 *     title="Desafio Hammer Consult Api - EndPoints",
 *     description="Mapeamento dos endpoints da api.",
 *     @OA\Contact(email="gabrielneres-12@hotmail.com")
 * )
 *
 * @OA\Server(url=L5_SWAGGER_CONST_HOST, description="Desafio Hammer Consult Api")
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Retorna o 'Objeto' serializado em 'json'.
     *
     * @param object $object
     * @param int $code
     *
     * @return string
     */
    protected function toJson($object, $code = 200)
    {
        $response = response(Utils::formatarJsonPadrao($object));
        $response->header('Content-Type', 'application/json');
        $response->header('charset', 'utf-8');

        if ($code != 200) {
            $response->setStatusCode($code);
        }

        return $response;
    }

    /**
     * Retorna a response 'OK'.
     *
     * @return Response
     */
    protected function ok()
    {
        $response = response()->make('');
        $response->setStatusCode(200);
        $response->setContent('OK');
        return $response;
    }
}
