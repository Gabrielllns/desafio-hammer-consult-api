<?php

namespace App\Http\Controllers;

use App\Business\EventoBO;
use App\Models\Evento;
use App\Util\Utils;
use Illuminate\Http\Request;

/**
 * Class EventoController
 *
 * @package App\Http\Controllers
 * @author Gabrielllns
 *
 * @OA\Tag(name="Eventos", description="Grupo de rotas referentes aos eventos.")
 */
class EventoController extends Controller
{
    /**
     * @var EventoBO
     */
    private $eventoBO;

    /**
     * EventoController constructor.
     *
     * @param EventoBO $eventoBO
     */
    public function __construct(EventoBO $eventoBO)
    {
        $this->eventoBO = $eventoBO;
    }

    /**
     * @OA\Post(
     *     path="/eventos",
     *     operationId="salvar",
     *     tags={"Eventos"},
     *     description="Salva os dados de evento.",
     *     @OA\Parameter(name="funcionario", in="path", required=true, @OA\Schema(type="Perfil")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Evento")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param Request $request
     * @return Evento
     */
    public function salvar(Request $request)
    {
        $evento = Utils::getRequestData($request->only(['data']));

        return $this->eventoBO->salvar($evento);
    }

    /**
     * @OA\Get(
     *     path="/eventos",
     *     operationId="getEventos",
     *     tags={"Eventos"},
     *     description="Retorna todos os eventos cadastrados.",
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Evento")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @return Evento[]
     */
    public function getEventos()
    {
        return $this->eventoBO->getEventos();
    }

    /**
     * @OA\Get(
     *     path="/eventos/{id}",
     *     operationId="getEventoPorId",
     *     tags={"Eventos"},
     *     description="Retorna o evento cadastrado por meio do 'id' informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Evento")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return Evento
     */
    public function getEventoPorId($id)
    {
        return $this->eventoBO->getEventoPorId($id);
    }
}
