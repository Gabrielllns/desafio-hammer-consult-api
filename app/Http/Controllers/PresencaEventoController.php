<?php

namespace App\Http\Controllers;

use App\Business\PresencaEventoBO;
use App\Models\Evento;
use App\Models\PresencaEvento;
use App\To\EstatisticaEventoTO;
use App\Util\Utils;
use Illuminate\Http\Request;

/**
 * Class PresencaEventoController
 *
 * @package App\Http\Controllers
 * @author Gabrielllns
 *
 * @OA\Tag(name="Presenças Eventos", description="Grupo de rotas referentes as presenças nos eventos.")
 */
class PresencaEventoController extends Controller
{
    /**
     * @var PresencaEventoBO
     */
    private $presencaEventoBO;

    /**
     * EventoController constructor.
     *
     * @param PresencaEventoBO $presencaEventoBO
     */
    public function __construct(PresencaEventoBO $presencaEventoBO)
    {
        $this->presencaEventoBO = $presencaEventoBO;
    }

    /**
     * @OA\Put(
     *     path="/presencas-eventos/{id}/cancelar",
     *     operationId="cancelar",
     *     tags={"Presenças Eventos"},
     *     description="Cancela a participação do usuário no evento.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="boolean")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return boolean
     */
    public function cancelar($id)
    {
        return $this->presencaEventoBO->cancelar($id);
    }

    /**
     * @OA\Post(
     *     path="/presencas-eventos",
     *     operationId="salvar",
     *     tags={"Presenças Eventos"},
     *     description="Salva a participação de um funcionário/convidado no evento.",
     *     @OA\Parameter(name="presencasEvento", in="path", required=true, @OA\Schema(type="PresencaEvento")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/PresencaEvento")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param Request $request
     * @return PresencaEvento
     */
    public function salvar(Request $request)
    {
        $presencasEvento = Utils::getRequestData($request->only(['data']));

        return $this->presencaEventoBO->salvar($presencasEvento);
    }

    /**
     * @OA\Get(
     *     path="/presencas-eventos/eventos/{id}/total-gasto/comida",
     *     operationId="getTotalGastoComidaEvento",
     *     tags={"Presenças Eventos"},
     *     description="Retorna o total gasto com comida no evento informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="float")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return float
     */
    public function getTotalGastoComidaEvento($id)
    {
        return $this->presencaEventoBO->getTotalGastoComidaEvento($id);
    }

    /**
     * @OA\Get(
     *     path="/presencas-eventos/eventos/{id}/total-gasto/bebida",
     *     operationId="getTotalGastoBebidaEvento",
     *     tags={"Presenças Eventos"},
     *     description="Retorna o total gasto com bebida no evento informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="float")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return float
     */
    public function getTotalGastoBebidaEvento($id)
    {
        return $this->presencaEventoBO->getTotalGastoBebidaEvento($id);
    }

    /**
     * @OA\Get(
     *     path="/presencas-eventos/eventos/{id}/total-gasto",
     *     operationId="getTotalGastoEvento",
     *     tags={"Presenças Eventos"},
     *     description="Retorna o total gasto com comida/bebida no evento informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="float")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return float
     */
    public function getTotalGastoEvento($id)
    {
        return $this->presencaEventoBO->getTotalGastoEvento($id);
    }

    /**
     * @OA\Get(
     *     path="/presencas-eventos/eventos/{id}/total-participantes/funcionario",
     *     operationId="getTotalFuncionariosConfirmadosEvento",
     *     tags={"Presenças Eventos"},
     *     description="Retorna o total de funcionários confirmados no evento informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="number")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return number
     */
    public function getTotalFuncionariosConfirmadosEvento($id)
    {
        return $this->presencaEventoBO->getTotalFuncionariosConfirmadosEvento($id);
    }

    /**
     * @OA\Get(
     *     path="/presencas-eventos/eventos/{id}/total-participantes/convidado",
     *     operationId="getTotalConvidadosConfirmadosEvento",
     *     tags={"Presenças Eventos"},
     *     description="Retorna o total de convidados confirmados no evento informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="number")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return number
     */
    public function getTotalConvidadosConfirmadosEvento($id)
    {
        return $this->presencaEventoBO->getTotalConvidadosConfirmadosEvento($id);
    }

    /**
     * @OA\Get(
     *     path="/presencas-eventos/eventos/{id}/total-participantes",
     *     operationId="getTotalConfirmadosEvento",
     *     tags={"Presenças Eventos"},
     *     description="Retorna o total de funcionários/convidados confirmados no evento informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="number")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return number
     */
    public function getTotalConfirmadosEvento($id)
    {
        return $this->presencaEventoBO->getTotalConfirmadosEvento($id);
    }

    /**
     * @OA\Get(
     *     path="/presencas-eventos/eventos/{id}/estatistica",
     *     operationId="getEstatisticasEvento",
     *     tags={"Presenças Eventos"},
     *     description="Retorna as estatísticas do evento informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="number")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return EstatisticaEventoTO|false
     */
    public function getEstatisticasEvento($id)
    {
        return $this->presencaEventoBO->getEstatisticasEvento($id);
    }
}
