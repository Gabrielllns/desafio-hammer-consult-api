<?php

namespace App\Http\Controllers;

use App\Business\UsuarioBO;
use App\Models\Usuario;

/**
 * Class UsuarioController
 *
 * @package App\Http\Controllers
 * @author Gabrielllns
 *
 * @OA\Tag(name="Usuários", description="Grupo de rotas referentes aos usuários.")
 */
class UsuarioController extends Controller
{
    /**
     * @var UsuarioBO
     */
    private $usuarioBO;

    /**
     * UsuarioController constructor.
     *
     * @param UsuarioBO $usuarioBO
     */
    public function __construct(UsuarioBO $usuarioBO)
    {
        $this->usuarioBO = $usuarioBO;
    }

    /**
     * @OA\Get(
     *     path="/usuarios",
     *     operationId="getUsuarios",
     *     tags={"Usuários"},
     *     description="Retorna todos os usuários cadastrados.",
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Usuario")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     */
    public function getUsuarios()
    {
        return $this->usuarioBO->getUsuarios();
    }

    /**
     * @OA\Get(
     *     path="/usuarios/{id}",
     *     operationId="getUsuarioPorId",
     *     tags={"Usuários"},
     *     description="Retorna o usuário cadastrado por meio do 'id' informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Usuario")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return Usuario
     */
    public function getUsuarioPorId($id)
    {
        return $this->usuarioBO->getUsuarioPorId($id);
    }
}
