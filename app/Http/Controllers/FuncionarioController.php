<?php

namespace App\Http\Controllers;

use App\Business\FuncionarioBO;
use App\Models\Funcionario;
use App\Util\Utils;
use Illuminate\Http\Request;

/**
 * Class FuncionarioController
 *
 * @package App\Http\Controllers
 * @author Gabrielllns
 *
 * @OA\Tag(name="Funcionários", description="Grupo de rotas referentes aos funcionários.")
 */
class FuncionarioController extends Controller
{
    /**
     * @var FuncionarioBO
     */
    private $funcionarioBO;

    /**
     * FuncionarioController constructor.
     *
     * @param FuncionarioBO $funcionarioBO
     */
    public function __construct(FuncionarioBO $funcionarioBO)
    {
        $this->funcionarioBO = $funcionarioBO;
    }

    /**
     * @OA\Post(
     *     path="/funcionarios",
     *     operationId="salvar",
     *     tags={"Funcionários"},
     *     description="Salva os dados de funcionário.",
     *     @OA\Parameter(name="funcionario", in="path", required=true, @OA\Schema(type="Funcionario")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Funcionario")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param Request $request
     * @return Funcionario
     */
    public function salvar(Request $request)
    {
        $funcionario = Utils::getRequestData($request->only(['data']));

        return $this->funcionarioBO->salvar($funcionario);
    }

    /**
     * @OA\Get(
     *     path="/funcionarios",
     *     operationId="getFuncionarios",
     *     tags={"Funcionários"},
     *     description="Retorna todos os funcionários cadastrados.",
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Funcionario")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @return Funcionario[]
     */
    public function getFuncionarios()
    {
        return $this->funcionarioBO->getFuncionarios();
    }

    /**
     * @OA\Get(
     *     path="/funcionarios/{id}",
     *     operationId="getFuncionarioPorId",
     *     tags={"Funcionários"},
     *     description="Retorna o funcionário cadastrado por meio do 'id' informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object", ref="#/components/schemas/Funcionario")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return Funcionario
     */
    public function getFuncionarioPorId($id)
    {
        return $this->funcionarioBO->getFuncionarioPorId($id);
    }

    /**
     * @OA\Get(
     *     path="/funcionarios/confirmados/evento/{id}",
     *     operationId="getFuncionariosConfirmadosPorEvento",
     *     tags={"Funcionários"},
     *     description="Retorna a lista de funcionários confirmados conforme o 'id' do evento informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object|array", ref="#/components/schemas/Funcionario")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return Funcionario[]
     */
    public function getFuncionariosConfirmadosPorEvento($id)
    {
        return $this->funcionarioBO->getFuncionariosConfirmadosPorEvento($id);
    }

    /**
     * @OA\Get(
     *     path="/funcionarios/pendentes/evento/{id}",
     *     operationId="getFuncionarioPendentesPorEvento",
     *     tags={"Funcionários"},
     *     description="Retorna a lista de funcionários que não confirmaram presença no evento informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="object|array", ref="#/components/schemas/Funcionario")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return Funcionario[]
     */
    public function getFuncionarioPendentesPorEvento($id)
    {
        return $this->funcionarioBO->getFuncionarioPendentesPorEvento($id);
    }

    /**
     * @OA\Get(
     *     path="/funcionarios/confirmados/evento/{id}/total",
     *     operationId="getTotalFuncionariosConfirmadosPorEvento",
     *     tags={"Funcionários"},
     *     description="Retorna o total de funcionários confirmados conforme o 'id' do evento informado.",
     *     @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Successo", @OA\JsonContent(type="integer")),
     *     @OA\Response(response=401, description="Acesso não autorizado"),
     *     @OA\Response(response=403, description="Acesso negado")
     * )
     *
     * @param integer $id
     * @return integer
     */
    public function getTotalFuncionariosConfirmadosPorEvento($id)
    {
        return $this->funcionarioBO->getTotalFuncionariosConfirmadosPorEvento($id);
    }
}
