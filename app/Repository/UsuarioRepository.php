<?php

namespace App\Repository;

use App\Models\Usuario;

/**
 * Classe que realizará as operações de persistência de dados referentes ao model 'Usuário'.
 *
 * @author Gabrielllns
 * @package App\Repository
 */
class UsuarioRepository
{

    /**
     * @var Usuario
     */
    private $usuario;

    /**
     * Construtor da classe.
     *
     * @param Usuario $usuario
     */
    public function __construct(Usuario $usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Retorna todos os usuários cadastrados.
     *
     * @return Usuario[]|object
     */
    public function getUsuarios()
    {
        return $this->usuario->with(['perfil'])->get();
    }

    /**
     * Retorna o usuário por meio do 'id' informado.
     *
     * @param integer $id
     * @return Usuario|object
     */
    public function getUsuarioPorId($id)
    {
        return $this->usuario->with(['perfil'])->find($id);
    }

    /**
     * Cria um novo usuário.
     *
     * @param array $usuario
     * @return Usuario|object|array
     */
    public function criar($usuario)
    {
        try {
            $usuario = $this->usuario->create($usuario);
        } catch (\Exception $e) {
            abort($e->getCode(), $e->getMessage());
        }

        return $usuario;
    }

    /**
     * Atualiza os dados do usuário.
     *
     * @param array $usuarioNovo
     * @return void
     */
    public function atualizar($usuarioNovo)
    {
        try {
            $usuario = $this->getUsuarioPorId($usuarioNovo['id']);

            $usuario->nome = $usuarioNovo['nome'];
            $usuario->id_perfil = $usuarioNovo['id_perfil'];

            $usuario->save();
        } catch (\Exception $e) {
            abort($e->getCode(), $e->getMessage());
        }
    }

    /**
     * Remove o usuário da base de dados por meio do 'id' informado.
     *
     * @param integer $idUsuario
     * @return boolean
     */
    public function deletarPorId($idUsuario)
    {
        return $this->usuario->where('id', '=', $idUsuario)->delete();
    }

}
