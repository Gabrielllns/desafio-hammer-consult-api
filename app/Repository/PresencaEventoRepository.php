<?php

namespace App\Repository;

use App\Models\PresencaEvento;
use App\Util\Constants;

/**
 * Classe que realizará as operações de persistência de dados referentes ao model 'PresencaPresencaEvento'.
 *
 * @author Gabrielllns
 * @package App\Repository
 */
class PresencaEventoRepository
{

    /**
     * @var PresencaEvento
     */
    private $presencaEvento;

    /**
     * Construtor da classe.
     *
     * @param PresencaEvento $presencaEvento
     */
    public function __construct(PresencaEvento $presencaEvento)
    {
        $this->presencaEvento = $presencaEvento;
    }

    /**
     * Recupera a instância da presença no evento conforme o 'id' informado.
     *
     * @param integer $idPresencaEvento
     * @return PresencaEvento|object
     */
    public function getPresencaEventoPorId($idPresencaEvento)
    {
        return $this->presencaEvento->with(['usuario'])->findOrFail($idPresencaEvento);
    }

    /**
     * Retorna o total gasto com comida no evento informado.
     *
     * @param integer $id
     * @return number
     */
    public function getTotalGastoComidaEvento($id)
    {
        return $this->presencaEvento->join('eventos', 'presenca_eventos.id_evento', '=', 'eventos.id')
            ->where('presenca_eventos.id_evento', '=', $id)
            ->count();
    }

    /**
     * Retorna o total gasto com bebida no evento informado.
     *
     * @param integer $id
     * @return number
     */
    public function getTotalGastoBebidaEvento($id)
    {
        return $this->presencaEvento->join('eventos', 'presenca_eventos.id_evento', '=', 'eventos.id')
            ->where('presenca_eventos.id_evento', '=', $id)->where('presenca_eventos.st_bebe', '=', true)
            ->count();
    }

    /**
     * Retorna o total de funcionários confirmados no evento informado.
     *
     * @param integer $id
     * @return number
     */
    public function getTotalFuncionariosConfirmadosEvento($id)
    {
        return $this->presencaEvento->join('eventos', 'presenca_eventos.id_evento', '=', 'eventos.id')
            ->join('usuarios', 'presenca_eventos.id_usuario', '=', 'usuarios.id')
            ->where('presenca_eventos.id_evento', '=', $id)
            ->where('usuarios.id_perfil', '<>', Constants::ID_PERFIL_CONVIDADO)
            ->count();
    }

    /**
     * Retorna o total de convidados confirmados no evento informado.
     *
     * @param integer $id
     * @return number
     */
    public function getTotalConvidadosConfirmadosEvento($id)
    {
        return $this->presencaEvento->join('eventos', 'presenca_eventos.id_evento', '=', 'eventos.id')
            ->join('usuarios', 'presenca_eventos.id_usuario', '=', 'usuarios.id')
            ->where('presenca_eventos.id_evento', '=', $id)
            ->where('usuarios.id_perfil', '=', Constants::ID_PERFIL_CONVIDADO)
            ->count();
    }

    /**
     * Salva a participação de um funcionário/convidado no evento.
     *
     * @param array $presencaEvento
     * @return PresencaEvento|object
     */
    public function criar($presencaEvento)
    {
        return $this->presencaEvento->create($presencaEvento);
    }
}
