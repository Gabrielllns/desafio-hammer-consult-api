<?php

namespace App\Repository;

use App\Models\Perfil;

/**
 * Classe que realizará as operações de persistência de dados referentes ao model 'Perfil'.
 *
 * @author Gabrielllns
 * @package App\Repository
 */
class PerfilRepository
{

    /**
     * @var Perfil
     */
    private $perfil;

    /**
     * Construtor da classe.
     *
     * @param Perfil $perfil
     */
    public function __construct(Perfil $perfil)
    {
        $this->perfil = $perfil;
    }

    /**
     * Retorna todos os perfis cadastrados.
     *
     * @return Perfil[]|object
     */
    public function getPerfis()
    {
        return $this->perfil->all();
    }

    /**
     * Retorna todos os perfis vinculados a empresa cadastrados.
     *
     * @return Perfil[]|object
     */
    public function getPerfisEmpresa()
    {
        return $this->perfil->where('st_empresa', '=', true)->get();
    }

    /**
     * Retorna o perfil cadastrado por meio do 'id' informado.
     *
     * @param integer $id
     *
     * @return Perfil|object
     */
    public function getPerfilPorId($id)
    {
        return $this->perfil->find($id);
    }

}
