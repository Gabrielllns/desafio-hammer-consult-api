<?php

namespace App\Repository;

use App\Models\Evento;

/**
 * Classe que realizará as operações de persistência de dados referentes ao model 'Evento'.
 *
 * @author Gabrielllns
 * @package App\Repository
 */
class EventoRepository
{

    /**
     * @var Evento
     */
    private $evento;

    /**
     * Construtor da classe.
     *
     * @param Evento $evento
     */
    public function __construct(Evento $evento)
    {
        $this->evento = $evento;
    }

    /**
     * Retorna todos os eventos cadastrados.
     *
     * @return Evento[]|object
     */
    public function getEventos()
    {
        return $this->evento->all();
    }

    /**
     * Retorna o evento por meio do 'id' informado.
     *
     * @param integer $id
     * @return Evento|object
     */
    public function getEventoPorId($id)
    {
        return $this->evento->with(['usuario'])->find($id);
    }

    /**
     * Cria um novo evento.
     *
     * @param array $evento
     * @return Evento|object|array
     */
    public function criar($evento)
    {
        try {
            $evento = $this->evento->create($evento);
        } catch (\Exception $e) {
            abort($e->getCode(), $e->getMessage());
        }

        return $evento;
    }

    /**
     * Atualiza os dados do evento.
     *
     * @param array $eventoNovo
     * @return void
     */
    public function atualizar($eventoNovo)
    {
        try {
            $evento = $this->getEventoPorId($eventoNovo['id']);

            $evento->titulo = $eventoNovo['titulo'];
            $evento->descricao = $eventoNovo['descricao'];
            $evento->data_realizacao = $eventoNovo['data_realizacao'];

            $evento->save();
        } catch (\Exception $e) {
            abort($e->getCode(), $e->getMessage());
        }
    }

}
