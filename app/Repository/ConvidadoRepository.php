<?php

namespace App\Repository;

use App\Models\Convidado;

/**
 * Classe que realizará as operações de persistência de dados referentes ao model 'Convidado'.
 *
 * @author Gabrielllns
 * @package App\Repository
 */
class ConvidadoRepository
{

    /**
     * @var Convidado
     */
    private $convidado;

    /**
     * Construtor da classe.
     *
     * @param Convidado $convidado
     */
    public function __construct(Convidado $convidado)
    {
        $this->convidado = $convidado;
    }

    /**
     * Retorna todos os convidados cadastrados.
     *
     * @return Convidado[]|object
     */
    public function getConvidados()
    {
        return $this->convidado->with(['usuario.perfil'])->get();
    }

    /**
     * Retorna o convidado cadastrado por meio do 'id' informado.
     *
     * @param integer $id
     * @return Convidado|object
     */
    public function getConvidadoPorId($id)
    {
        return $this->convidado->with(['usuario.perfil'])->find($id);
    }

    /**
     * Salva o convidado da base de dados.
     *
     * @param array $convidado
     * @return Convidado|object
     */
    public function criar($convidado)
    {
        return $this->convidado->create($convidado);
    }

    /**
     * Remove o convidado da base de dados por meio do 'id' do usuário informado.
     *
     * @param integer $idUsuario
     * @return boolean
     */
    public function deletarPorIdUsuario($idUsuario)
    {
        return $this->convidado->where('id_usuario', '=', $idUsuario)->delete();
    }

    /**
     * Retorna a lista de convidados confirmados conforme o 'id' do evento informado.
     *
     * @param integer $id
     * @return Convidado[]|object[]
     */
    public function getConvidadosConfirmadosPorEvento($id)
    {
        return $this->convidado->with(['usuario.presencaEvento', 'funcionarioResponsavel.usuario'])->whereHas(
            'usuario.presencaEvento', function ($query) use ($id) {
            $query->where('id_evento', '=', $id);
        })->get();
    }

    /**
     * Retorna o total de convidados confirmados conforme o 'id' do evento informado.
     *
     * @param integer $id
     * @return integer
     */
    public function getTotalConvidadosConfirmadosPorEvento($id)
    {
        return $this->convidado->whereHas('usuario.presencaEvento', function ($query) use ($id) {
            $query->where('id_evento', '=', $id);
        })->count();
    }
}
