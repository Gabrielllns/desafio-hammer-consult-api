<?php

namespace App\Repository;

use App\Models\Funcionario;

/**
 * Classe que realizará as operações de persistência de dados referentes ao model 'Funcionário'.
 *
 * @author Gabrielllns
 * @package App\Repository
 */
class FuncionarioRepository
{

    /**
     * @var Funcionario
     */
    private $funcionario;

    /**
     * Construtor da classe.
     *
     * @param Funcionario $funcionario
     */
    public function __construct(Funcionario $funcionario)
    {
        $this->funcionario = $funcionario;
    }

    /**
     * Retorna todos os funcionários cadastrados.
     *
     * @return Funcionario[]|object
     */
    public function getFuncionarios()
    {
        return $this->funcionario->with(['usuario.perfil'])->get();
    }

    /**
     * Retorna o funcionário cadastrado por meio do 'id' informado.
     *
     * @param integer $id
     * @return Funcionario|object
     */
    public function getFuncionarioPorId($id)
    {
        return $this->funcionario->with(['usuario.perfil'])->find($id);
    }

    /**
     * Salva o funcionário da base de dados.
     *
     * @param array $funcionario
     * @return Funcionario|object
     */
    public function criar($funcionario)
    {
        return $this->funcionario->create($funcionario);
    }

    /**
     * Retorna a lista de funcionários confirmados conforme o 'id' do evento informado.
     *
     * @param integer $id
     * @return Funcionario[]|object[]
     */
    public function getFuncionariosConfirmadosPorEvento($id)
    {
        return $this->funcionario->with(['usuario.presencaEvento', 'convidado'])->whereHas(
            'usuario.presencaEvento', function ($query) use ($id) {
            $query->where('id_evento', '=', $id);
        })->get();
    }

    /**
     * Retorna a lista de funcionários que não confirmaram presença no evento informado.
     *
     * @param integer $id
     * @return Funcionario[]|object[]
     */
    public function getFuncionarioPendentesPorEvento($id)
    {
        return $this->funcionario->with(['usuario'])->whereDoesntHave(
            'usuario.presencaEvento', function ($query) use ($id) {
            $query->where('id_evento', '=', $id);
        })->get();
    }

    /**
     * Retorna o total de funcionários confirmados conforme o 'id' do evento informado.
     *
     * @param integer $id
     * @return integer
     */
    public function getTotalFuncionariosConfirmadosPorEvento($id)
    {
        return $this->funcionario->whereHas('usuario.presencaEvento', function ($query) use ($id) {
            $query->where('id_evento', '=', $id);
        })->count();
    }
}
