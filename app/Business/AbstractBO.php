<?php

namespace App\Business;

use App\Repository\ConvidadoRepository;
use App\Repository\EventoRepository;
use App\Repository\FuncionarioRepository;
use App\Repository\PerfilRepository;
use App\Repository\PresencaEventoRepository;
use App\Repository\UsuarioRepository;

/**
 * Class AbstractBO.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class AbstractBO
{
    /**
     * @var UsuarioRepository
     */
    private $usuarioRepository;

    /**
     * @var FuncionarioRepository
     */
    private $funcionarioRepository;

    /**
     * @var ConvidadoRepository
     */
    private $convidadoRepository;

    /**
     * @var PerfilRepository
     */
    private $perfilRepository;

    /**
     * @var EventoRepository
     */
    private $eventoRepository;

    /**
     * @var PresencaEventoRepository
     */
    private $presencaEventoRepository;

    /**
     * Verifica se a APÍ esta em modo DEBUG.
     *
     * @return boolean
     */
    protected function isApiModeDebug()
    {
        return env('APP_DEBUG');
    }

    /**
     * Recupera o e-mail para qual deve ser enviado o e-mail conforme o modo da API.
     *
     * @param string $email
     *
     * @return string
     */
    protected function getEmailDefaultSend($email)
    {
        return (env('APP_DEBUG')) ? env('MAIL_DEFAULT') : $email;
    }

    /**
     * Retorna a instância de 'UsuarioRepository'.
     * Obs: A instância não foi obtida através do construtor para evitar o problema de referência ciclica.
     *
     * @return UsuarioRepository
     */
    protected function getUsuarioRepository()
    {
        if (is_null($this->usuarioRepository)) {
            $this->usuarioRepository = app()->make(UsuarioRepository::class);
        }

        return $this->usuarioRepository;
    }

    /**
     * Retorna a instância de 'FuncionarioRepository'.
     * Obs: A instância não foi obtida através do construtor para evitar o problema de referência ciclica.
     *
     * @return FuncionarioRepository
     */
    protected function getFuncionarioRepository()
    {
        if (is_null($this->funcionarioRepository)) {
            $this->funcionarioRepository = app()->make(FuncionarioRepository::class);
        }

        return $this->funcionarioRepository;
    }

    /**
     * Retorna a instância de 'ConvidadoRepository'.
     * Obs: A instância não foi obtida através do construtor para evitar o problema de referência ciclica.
     *
     * @return ConvidadoRepository
     */
    protected function getConvidadoRepository()
    {
        if (is_null($this->convidadoRepository)) {
            $this->convidadoRepository = app()->make(ConvidadoRepository::class);
        }

        return $this->convidadoRepository;
    }

    /**
     * Retorna a instância de 'PerfilRepository'.
     * Obs: A instância não foi obtida através do construtor para evitar o problema de referência ciclica.
     *
     * @return PerfilRepository
     */
    protected function getPerfilRepository()
    {
        if (is_null($this->perfilRepository)) {
            $this->perfilRepository = app()->make(PerfilRepository::class);
        }

        return $this->perfilRepository;
    }

    /**
     * Retorna a instância de 'EventoRepository'.
     * Obs: A instância não foi obtida através do construtor para evitar o problema de referência ciclica.
     *
     * @return EventoRepository
     */
    protected function getEventoRepository()
    {
        if (is_null($this->eventoRepository)) {
            $this->eventoRepository = app()->make(EventoRepository::class);
        }

        return $this->eventoRepository;
    }

    /**
     * Retorna a instância de 'PresencaEventoRepository'.
     * Obs: A instância não foi obtida através do construtor para evitar o problema de referência ciclica.
     *
     * @return PresencaEventoRepository
     */
    protected function getPresencaEventoRepository()
    {
        if (is_null($this->presencaEventoRepository)) {
            $this->presencaEventoRepository = app()->make(PresencaEventoRepository::class);
        }

        return $this->presencaEventoRepository;
    }
}
