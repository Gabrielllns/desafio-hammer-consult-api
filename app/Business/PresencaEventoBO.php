<?php

namespace App\Business;

use App\Models\PresencaEvento;
use App\To\EstatisticaEventoTO;
use App\Util\Constants;
use Illuminate\Support\Facades\DB;

/**
 * Class PresencaEventoBO.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class PresencaEventoBO extends AbstractBO
{
    const VALOR_ITEM_PARTICIPACAO = 10.00;

    /**
     * Cancela a participação do usuário no evento.
     *
     * @param integer $idPresencaEvento
     * @return boolean
     */
    public function cancelar($idPresencaEvento)
    {
        $isOk = false;

        try {
            DB::beginTransaction();

            $presencaEvento = $this->getPresencaEventoRepository()->getPresencaEventoPorId($idPresencaEvento);

            if ($presencaEvento->usuario->id_perfil == Constants::ID_PERFIL_CONVIDADO) {
                $this->getConvidadoRepository()->deletarPorIdUsuario($presencaEvento->usuario->id);

                $isOk = $presencaEvento->delete();

                $this->getUsuarioRepository()->deletarPorId($presencaEvento->usuario->id);
            } else {
                $isOk = $presencaEvento->delete();
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            abort($e->getCode(), $e->getMessage());
        }

        return $isOk;
    }

    /**
     * Salva a participação de um funcionário/convidado no evento.
     *
     * @param array $presencasEvento
     * @return PresencaEvento|PresencaEvento[]
     */
    public function salvar($presencasEvento)
    {
        try {
            DB::beginTransaction();

            $funcionario = $presencasEvento['funcionario'];

            if (!isset($funcionario['id'])) {
                $presencaEvento['id_usuario'] = $funcionario['id_usuario'];
                $presencaEvento['id_evento'] = $presencasEvento['id_evento'];
                $presencaEvento['st_bebe'] = (isset($funcionario['st_bebe']));

                $this->getPresencaEventoRepository()->criar($presencaEvento);
            }

            if (isset($presencasEvento['has_convidado']) && isset($funcionario['id'])) {
                $convidado = $presencasEvento['convidado'];

                // Persiste os dados de Usuário
                $usuario = $convidado['usuario'];
                $usuario['id_perfil'] = Constants::ID_PERFIL_CONVIDADO;

                $usuario = $this->getUsuarioRepository()->criar($usuario);

                // Persiste os dados de Convidado
                $usuarioConvidado['id_usuario'] = $usuario->id;
                $usuarioConvidado['id_funcionario_responsavel'] = $funcionario['id'];

                $usuarioConvidado = $this->getConvidadoRepository()->criar($usuarioConvidado);

                // Persiste os dados da Presença no Evento
                $presencaEvento['id_usuario'] = $usuarioConvidado->id_usuario;
                $presencaEvento['id_evento'] = $presencasEvento['id_evento'];
                $presencaEvento['st_bebe'] = (isset($convidado['st_bebe']));

                $this->getPresencaEventoRepository()->criar($presencaEvento);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            abort($e->getCode(), $e->getMessage());
        }

        return $presencasEvento;
    }


    /**
     * Retorna o total gasto com comida no evento informado.
     *
     * @param integer $id
     * @return float
     */
    public function getTotalGastoComidaEvento($id)
    {
        $total = ($this->getPresencaEventoRepository()->getTotalGastoComidaEvento($id) * self::VALOR_ITEM_PARTICIPACAO);
        return floatval(number_format($total, 2, '.', ''));
    }

    /**
     * Retorna o total gasto com bebida no evento informado.
     *
     * @param integer $id
     * @return float
     */
    public function getTotalGastoBebidaEvento($id)
    {
        $total = ($this->getPresencaEventoRepository()->getTotalGastoBebidaEvento($id) * self::VALOR_ITEM_PARTICIPACAO);
        return floatval(number_format($total, 2, '.', ''));
    }

    /**
     * Retorna o total gasto com comida/bebida no evento informado.
     *
     * @param integer $id
     * @return float
     */
    public function getTotalGastoEvento($id)
    {
        $total = $this->getTotalGastoBebidaEvento($id) + $this->getTotalGastoComidaEvento($id);
        return floatval(number_format($total, 2, '.', ''));
    }

    /**
     * Retorna o total de funcionários confirmados no evento informado.
     *
     * @param integer $id
     * @return number
     */
    public function getTotalFuncionariosConfirmadosEvento($id)
    {
        return $this->getPresencaEventoRepository()->getTotalFuncionariosConfirmadosEvento($id);
    }

    /**
     * Retorna o total de convidados confirmados no evento informado.
     *
     * @param integer $id
     * @return number
     */
    public function getTotalConvidadosConfirmadosEvento($id)
    {
        return $this->getPresencaEventoRepository()->getTotalConvidadosConfirmadosEvento($id);
    }

    /**
     * Retorna o total de funcionários/convidados confirmados no evento informado.
     *
     * @param integer $id
     * @return number
     */
    public function getTotalConfirmadosEvento($id)
    {
        return $this->getTotalFuncionariosConfirmadosEvento($id) + $this->getTotalConvidadosConfirmadosEvento($id);
    }

    /**
     * Retorna as estatísticas do evento informado.
     *
     * @param integer $id
     * @return EstatisticaEventoTO|false
     */
    public function getEstatisticasEvento($id)
    {
        $estatisticaEventoTO = new EstatisticaEventoTO();
        $estatisticaEventoTO->setTotalConvidadosConfirmados($this->getTotalConvidadosConfirmadosEvento($id));
        $estatisticaEventoTO->setTotalFuncionariosConfirmados($this->getTotalFuncionariosConfirmadosEvento($id));
        $estatisticaEventoTO->setTotalGasto($this->getTotalGastoBebidaEvento($id));
        $estatisticaEventoTO->setTotalArrecadado($this->getTotalGastoBebidaEvento($id));
        $estatisticaEventoTO->setTotalGastoBebida($this->getTotalGastoBebidaEvento($id));
        $estatisticaEventoTO->setTotalGastoComida($this->getTotalGastoComidaEvento($id));

        return $estatisticaEventoTO->compact();
    }
}
