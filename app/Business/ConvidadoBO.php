<?php

namespace App\Business;

use App\Models\Convidado;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class ConvidadoBO.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class ConvidadoBO extends AbstractBO
{
    /**
     * Salva os dados de convidado.
     *
     * @param array $convidado
     * @return Convidado
     */
    public function salvar($convidado)
    {
        if (empty($convidado['id'])) {
            $convidado = $this->criar($convidado);
        } else {
            $convidado = $this->atualizar($convidado);
        }

        return $convidado;
    }

    /**
     * Cria um novo convidado.
     *
     * @param array $convidado
     * @return Convidado|object
     */
    private function criar($convidado)
    {
        try {
            DB::beginTransaction();

            $usuario = $this->getUsuarioRepository()->criar($convidado['usuario']);

            $convidado['id_usuario'] = $usuario->id;
            $convidado['codigo'] = Carbon::now()->micro;

            $convidado = $this->getConvidadoRepository()->criar($convidado);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            abort($e->getCode(), $e->getMessage());
        }

        return $convidado;
    }

    /**
     * Atualiza os dados de um convidado.
     *
     * @param array $convidado
     * @return Convidado|object
     */
    private function atualizar($convidado)
    {
        try {
            DB::beginTransaction();

            $this->getUsuarioRepository()->atualizar($convidado['usuario']);

            $convidado = $this->getConvidadoRepository()->getConvidadoPorId($convidado['id']);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            abort($e->getCode(), $e->getMessage());
        }

        return $convidado;
    }

    /**
     * Retorna todos os convidados cadastrados.
     *
     * @return Convidado[]
     */
    public function getConvidados()
    {
        return $this->getConvidadoRepository()->getConvidados();
    }

    /**
     * Retorna o convidado cadastrado por meio do 'id' informado.
     *
     * @param integer $id
     * @return Convidado
     */
    public function getConvidadoPorId($id)
    {
        return $this->getConvidadoRepository()->getConvidadoPorId($id);
    }

    /**
     * Retorna a lista de convidados confirmados conforme o 'id' do evento informado.
     *
     * @param integer $id
     * @return Convidado[]
     */
    public function getConvidadosConfirmadosPorEvento($id)
    {
        return $this->getConvidadoRepository()->getConvidadosConfirmadosPorEvento($id);
    }

    /**
     * Retorna o total de convidados confirmados conforme o 'id' do evento informado.
     *
     * @param integer $id
     * @return integer
     */
    public function getTotalConvidadosConfirmadosPorEvento($id)
    {
        return $this->getConvidadoRepository()->getTotalConvidadosConfirmadosPorEvento($id);
    }
}
