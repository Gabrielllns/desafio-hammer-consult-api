<?php

namespace App\Business;

use App\Models\Funcionario;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class FuncionarioBO.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class FuncionarioBO extends AbstractBO
{
    /**
     * Salva os dados de funcionário.
     *
     * @param array $funcionario
     * @return Funcionario
     */
    public function salvar($funcionario)
    {
        if (empty($funcionario['id'])) {
            $funcionario = $this->criar($funcionario);
        } else {
            $funcionario = $this->atualizar($funcionario);
        }

        return $funcionario;
    }

    /**
     * Cria um novo funcionário.
     *
     * @param array $funcionario
     * @return Funcionario|object
     */
    private function criar($funcionario)
    {
        try {
            DB::beginTransaction();

            $usuario = $this->getUsuarioRepository()->criar($funcionario['usuario']);

            $funcionario['id_usuario'] = $usuario->id;
            $funcionario['codigo'] = Carbon::now()->micro;

            $funcionario = $this->getFuncionarioRepository()->criar($funcionario);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            abort($e->getCode(), $e->getMessage());
        }

        return $funcionario;
    }

    /**
     * Atualiza os dados de um funcionário.
     *
     * @param array $funcionario
     * @return Funcionario|object
     */
    private function atualizar($funcionario)
    {
        try {
            DB::beginTransaction();

            $this->getUsuarioRepository()->atualizar($funcionario['usuario']);

            $funcionario = $this->getFuncionarioRepository()->getFuncionarioPorId($funcionario['id']);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            abort($e->getCode(), $e->getMessage());
        }

        return $funcionario;
    }

    /**
     * Retorna todos os funcionários cadastrados.
     *
     * @return Funcionario[]
     */
    public function getFuncionarios()
    {
        return $this->getFuncionarioRepository()->getFuncionarios();
    }

    /**
     * Retorna o funcionário cadastrado por meio do 'id' informado.
     *
     * @param integer $id
     * @return Funcionario
     */
    public function getFuncionarioPorId($id)
    {
        return $this->getFuncionarioRepository()->getFuncionarioPorId($id);
    }

    /**
     * Retorna a lista de funcionários confirmados conforme o 'id' do evento informado.
     *
     * @param integer $id
     * @return Funcionario[]
     */
    public function getFuncionariosConfirmadosPorEvento($id)
    {
        return $this->getFuncionarioRepository()->getFuncionariosConfirmadosPorEvento($id);
    }

    /**
     * Retorna a lista de funcionários que não confirmaram presença no evento informado.
     *
     * @param integer $id
     * @return Funcionario[]
     */
    public function getFuncionarioPendentesPorEvento($id)
    {
        return $this->getFuncionarioRepository()->getFuncionarioPendentesPorEvento($id);
    }

    /**
     * Retorna o total de funcionários confirmados conforme o 'id' do evento informado.
     *
     * @param integer $id
     * @return integer
     */
    public function getTotalFuncionariosConfirmadosPorEvento($id)
    {
        return $this->getFuncionarioRepository()->getTotalFuncionariosConfirmadosPorEvento($id);
    }
}
