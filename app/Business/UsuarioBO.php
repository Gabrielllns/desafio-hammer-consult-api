<?php

namespace App\Business;

use App\Models\Usuario;

/**
 * Class UsuarioBO.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class UsuarioBO extends AbstractBO
{
    /**
     * Retorna todos os usuários cadastrados.
     *
     * @return Usuario[]
     */
    public function getUsuarios()
    {
        return $this->getUsuarioRepository()->getUsuarios();
    }

    /**
     * Retorna o usuário cadastrado por meio do 'id' informado.
     *
     * @param integer $id
     *
     * @return Usuario
     */
    public function getUsuarioPorId($id)
    {
        return $this->getUsuarioRepository()->getUsuarioPorId($id);
    }
}
