<?php

namespace App\Business;

use App\Models\Evento;
use App\Util\Constants;
use Carbon\Carbon;
use Carbon\CarbonInterface;
use Illuminate\Support\Facades\DB;

/**
 * Class EventoBO.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class EventoBO extends AbstractBO
{
    /**
     * Salva os dados de evento.
     *
     * @param array $evento
     * @return Evento
     */
    public function salvar($evento)
    {
        if (empty($evento['id'])) {
            $evento = $this->criar($evento);
        } else {
            $evento = $this->atualizar($evento);
        }

        return $evento;
    }

    /**
     * Cria um novo evento.
     *
     * @param array $evento
     * @return Evento|object
     */
    private function criar($evento)
    {
        try {
            DB::beginTransaction();

            $evento['id_usuario_responsavel'] = Constants::ID_USUARIO_RESPONSAVEL;
            $evento['data_realizacao'] = $this->getFullDataRealizacao($evento);

            $evento = $this->getEventoRepository()->criar($evento);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            abort($e->getCode(), $e->getMessage());
        }

        return $evento;
    }

    /**
     * Retorna a data de realização no padrão correto para a persistência.
     *
     * @param array $evento
     * @return CarbonInterface
     */
    private function getFullDataRealizacao($evento)
    {
        return Carbon::parse($evento['data_realizacao'] . ' ' . $evento['horario_realizacao'] . ':00', 'UTC');
    }

    /**
     * Atualiza os dados de um evento.
     *
     * @param array $evento
     * @return Evento|object
     */
    private function atualizar($evento)
    {
        try {
            DB::beginTransaction();

            $evento['data_realizacao'] = $this->getFullDataRealizacao($evento);

            $this->getEventoRepository()->atualizar($evento);

            $evento = $this->getEventoRepository()->getEventoPorId($evento['id']);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            abort($e->getCode(), $e->getMessage());
        }

        return $evento;
    }

    /**
     * Retorna todos os eventos cadastrados.
     *
     * @return Evento[]
     */
    public function getEventos()
    {
        return $this->getEventoRepository()->getEventos();
    }

    /**
     * Retorna o evento cadastrado por meio do 'id' informado.
     *
     * @param integer $id
     * @return Evento
     */
    public function getEventoPorId($id)
    {
        return $this->getEventoRepository()->getEventoPorId($id);
    }
}
