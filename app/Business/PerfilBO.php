<?php

namespace App\Business;

use App\Models\Perfil;

/**
 * Class PerfilBO.
 *
 * @package App\Business
 * @author Gabrielllns
 */
class PerfilBO extends AbstractBO
{
    /**
     * Retorna todos os perfis cadastrados.
     *
     * @return Perfil[]
     */
    public function getPerfis()
    {
        return $this->getPerfilRepository()->getPerfis();
    }

    /**
     * Retorna todos os perfis vinculados a empresa cadastrados.
     *
     * @return Perfil[]
     */
    public function getPerfisEmpresa()
    {
        return $this->getPerfilRepository()->getPerfisEmpresa();
    }

    /**
     * Retorna o perfil cadastrado por meio do 'id' informado.
     *
     * @param integer $id
     *
     * @return Perfil
     */
    public function getPerfilPorId($id)
    {
        return $this->getPerfilRepository()->getPerfilPorId($id);
    }
}
