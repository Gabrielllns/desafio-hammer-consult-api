# PROBLEMA #

Aqui na hammer organizamos um churras todos os meses, **é necessario que cada funcionario contribua 20$ e se quiser levar um convidado 40$**.
O problema é que não temos uma maneira para controlar quem irá no churrasco e quanto foi gasto com comida e bebida.
**Um funcionário pode levar apenas um convidado,se você não beber,o valor do churrasco será metade,a mesma regra é valida para o convidado.**
Dado o problema é necessario desenvolver uma aplicação WebApi para consumo do nosso front.

### Dependências: ###

* PHP >= 7.2.5
* Composer >= 1.10

### Tecnologias: ###

* **Front-end:** Gerado com [Angular CLI](https://github.com/angular/angular-cli) em sua versão 10.0.2.
* **Back-end:** Gerado com [Laravel Framework](https://github.com/laravel/laravel) em sua versão 7.x. 
* **Banco de Dados:** Mysql.

### Executando o projeto: ###

* Clonar o projeto;
* Entrar na pasta do projeto;
* Instalar as dependências através do comando `composer install`;
* Executar o comando `php -S localhost:8080 -t public`;
* Em seguida já pode acessar a aplicação através da url: `http://localhost:8080/`

Caso queira fazer uso das Migrations:

* Configurar o arquivo `.env` com as configurações do banco;
* Criar a tabela informada no arquivo de configuração na base de dados;
* Executar o comando `php artisan migrate --seed`

## Informações ###

Para mais informações execute no terminal o comando `laravel help`, `php artisan help` ou consulte [Laravel Doc](hhttps://laravel.com/docs/7.x).