<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Gabrielllns"/>
    <meta name="keywords" content="DesafioHammerConsultApi"/>
    <meta name="description" content="DesafioHammerConsultApi"/>

    <title>Desafio Hammer Consult Api</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="https://www.secret-source.eu/wp-content/uploads/2017/11/Laravel-logo.jpg">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        a {
            color: #636B6F;
            text-decoration: none;
        }

        a:hover {
            color: #9ca4a9;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            <a href="/api/" title="Desafio Hammer Consult Api - EndPoints">Desafio Hammer Consult Api</a>
        </div>
    </div>
</div>
</body>
</html>
